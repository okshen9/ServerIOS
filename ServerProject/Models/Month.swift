//
//  Month.swift
//  ServerProject
//
//  Created by Artem Neshko on 5/24/20.
//  Copyright © 2020 Artem Neshko. All rights reserved.
//

import Foundation
class Month: Entity {
    var number: Int
    var days: [Day] = []
    
    var randPer = Int.random(in: 0...100)
    var randPerStat: Int {
        let aa = randPer + Int.random(in: -15...15) % 100
        if aa > 100 {
            return Int.random(in: 90...99)
        }
        
        return abs(aa)
    }
    
    var persent: Int = Int.random(in: 0...100)
    //    {
    //        var allPersent = 0.0
    //        for hour in self.days {
    //            allPersent += Double(hour.persent)
    //        }
    //        return Int(allPersent / Double(days.count))
    //    }
    var currentPersent: Int
    //    {
    //        let aa = persent + Int.random(in: -15...15) % 100
    //       print(persent, aa, "---")
    //        if aa > 100 {
    //            return Int.random(in: 90...99)
    //        }
    //
    //        return abs(aa)
    //    }
    //    {
    //        var allPersent = 0.0
    //        for hour in self.days {
    //            allPersent += Double(hour.currentPersent)
    //        }
    //        return Int(allPersent / Double(days.count))
    //    }
    
    var countDay: Int {return _countsDay[number]}
    
    private let _countsDay = [0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    var coefficients = [0.0]
    
    var name: String {
        switch number {
        case 1:
            return TCLLocalizedStrings.january.localized
        case 2:
            return TCLLocalizedStrings.february.localized
        case 3:
            return TCLLocalizedStrings.march.localized
        case 4:
            return TCLLocalizedStrings.april.localized
        case 5:
            return TCLLocalizedStrings.may.localized
        case 6:
            return TCLLocalizedStrings.june.localized
        case 7:
            return TCLLocalizedStrings.july.localized
        case 8:
            return TCLLocalizedStrings.august.localized
        case 9:
            return TCLLocalizedStrings.september.localized
        case 10:
            return TCLLocalizedStrings.october.localized
        case 11:
            return TCLLocalizedStrings.november.localized
        case 12:
            return TCLLocalizedStrings.december.localized
        default:
            return TCLLocalizedStrings.noName.localized
        }
    }
    
    init(index: Int) {
        
        let aa = persent + Int.random(in: -15...15) % 100
        print(persent, aa, "---")
        if aa > 100 {
            self.currentPersent = Int.random(in: 90...99)
        } else {
            self.currentPersent =  abs(aa)
        }
        
        number = index
        for indexDay in 1...countDay + 1 {
            coefficients.append(Double.random(in: 0.7...1.3))
            days.append(Day(index: indexDay, coefficient: coefficients[indexDay], monthName: name))
            
        }
        print("---", coefficients)
    }
    
}

class Day: Entity {
    var number: Int
    var hours = [Hour]()
    
    var randPer = Int.random(in: 0...100)
    var randPerStat: Int {
        let aa = randPer + Int.random(in: -15...15) % 100
        if aa > 100 {
            return Int.random(in: 90...99)
        }
        return abs(aa)
    }
    
    //    var persent: Int = Int.random(in: 0...100)
    //    //    {
    //    //        var allPersent = 0.0
    //    //        for hour in self.hours {
    //    //            allPersent += Double(hour.persent)
    //    //        }
    //    //        return Int(allPersent / Double(hours.count))
    //    //    }
    //    var currentPersent: Int = Int.random(in: 0...100)
    //    //    {
    //    //        var allPersent = 0.0
    //    //        for hour in self.hours {
    //    //            allPersent += Double(hour.currentPersent)
    //    //        }
    //    //        return Int(allPersent / Double(hours.count))
    //    //    }
    
    var persent: Int = Int.random(in: 0...100)
    //    {
    //        var allPersent = 0.0
    //        for hour in self.days {
    //            allPersent += Double(hour.persent)
    //        }
    //        return Int(allPersent / Double(days.count))
    //    }
    var currentPersent: Int
    //        {
    //            let aa = persent + Int.random(in: -15...15) % 100
    //           print(persent, aa, "---")
    //            if aa > 100 {
    //                return Int.random(in: 90...99)
    //            }
    //
    //            return abs(aa)
    //        }
    let _monthName: String
    var name: String {
        return _monthName + "\(number)"
    }
    
    init(index: Int, coefficient: Double, monthName: String) {
        
        _monthName = monthName + ", "
        
        let aa = persent + Int.random(in: -15...15) % 100
        if aa > 100 {
            self.currentPersent = Int.random(in: 90...99)
        } else {
            self.currentPersent =  abs(aa)
        }
        
        number = index
        for index in 0...24 {
            hours.append(Hour(index: index, coefficient: coefficient))
        }
    }
    
}

class Hour: Entity {
    
    var randPer = Int.random(in: 0...100)
    var randPerStat: Int {
        let aa = randPer + Int.random(in: -15...15) % 100
        if aa > 100 {
            return Int.random(in: 90...99)
        }
        return abs(aa)
    }
    
    var number: Int
    var name: String {
        return " \(number):00 "
    }
    var persent: Int
    var currentPersent: Int
    
    init(index: Int, coefficient: Double) {
        self.number = index
        let per = Int.random(in: 0...100)
//        if Int(Double(per) * coefficient) <= 100 {
//            self.persent = Int(Double(Int.random(in: 0...100)) * coefficient)
//        } else {
//            self.persent = Int.random(in: 0...100)
//        }
//        self.currentPersent = persent + Int.random(in: -30...30)
        self.persent = Int.random(in: 0...100)
        let aa = persent + Int.random(in: -15...15) % 100
        if aa > 100 {
            self.currentPersent = Int.random(in: 90...99)
        } else {
            self.currentPersent =  abs(aa)
        }
    }
}


protocol Entity {
    //    var randPer: Int { get set }
    //    var randPerStat: Int { get }
    
    var number: Int { get set }
    var name: String { get }
    var persent: Int { get }
    var currentPersent: Int { get set }
}
