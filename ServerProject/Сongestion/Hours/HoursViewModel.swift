//
//  HoursViewModel.swift
//  ServerProject
//
//  Created by Artem Neshko on 15.06.2020.
//  Copyright © 2020 Artem Neshko. All rights reserved.
//

import Foundation
import RxSwift
import RxDataSources
import RxCocoa

class HoursViewModel {
    
    let db = DisposeBag()
    
    var sections = BehaviorRelay<[SectionModel]>(value: [.activitySection(items: [.activityItem])])
    
    //private
    var month: Day?
    var halls: [Hour]?
    var nameMonth = BehaviorRelay(value: "Название месяца")
    
    init(_ month: Day) {
        self.month = month
        halls = month.hours
        sections.accept(self.generateSectionsFrom(month.hours))
        nameMonth.accept(month.name )
    }
    
    
    func languageChanged() {
        guard self.halls != nil else { return }
        sections.accept(self.generateLoadingSections())
        
        print("rrr")
    }
    
    func getData(showActivityIndicator: Bool = true, completed: @escaping () -> ()) {
        if showActivityIndicator {
            sections.accept(self.generateLoadingSections())
        }
        guard let days = halls else {
            return completed()
        }
        Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false) { timer in
            self.sections.accept(self.generateSectionsFrom(days))
            completed()
        }
    }
    
    private func generateSectionsFrom(_ halls: [Hour]) -> [SectionModel] {
        self.halls = halls
        var sections: [SectionModel] = []
        guard !halls.isEmpty else {
            return [SectionModel]()
        }
        
        sections.append(SectionModel.chartsSection(items: [ItemModel.chartItem(monthes: self.halls ?? [])]))
        sections.append(SectionModel.chartsSection(items: [ItemModel.myChartItem(hours: self.halls ?? [])]))
        sections.append(SectionModel.hallsNearSection(items: [ItemModel.hallsNearItem(ident: TCLLocalizedStrings.currentLanguage.identifier)]))
        
        for hall in halls {
            sections.append(SectionModel.hallSection(items: [ItemModel.hallItem(hall: hall, persent: hall.persent, ident: hall.name )]))
        }
        
        return sections
    }
    
    
    private func generateLoadingSections() -> [SectionModel] {
        return [
            .hallsNearSection(items: [.hallsNearItem(ident: TCLLocalizedStrings.currentLanguage.identifier)]),
            .activitySection(items: [.activityItem])
        ]
    }
    
    private func generateBaseSections() -> [SectionModel] {
        return [
            .hallsNearSection(items: [.hallsNearItem(ident: TCLLocalizedStrings.currentLanguage.identifier)]),
            .activitySection(items: [.activityItem])
        ]
    }
    
    enum SectionModel: AnimatableSectionModelType {
        
        case chartsSection(items: [Item])
        case hallsNearSection(items: [Item])
        case hallSection(items: [Item])
        case activitySection(items: [Item])
        case myChartSection(items: [Item])
        
        typealias Item = ItemModel
        typealias Identity = String
        
        var items: [Item] {
            switch self {
            case .chartsSection(items: let items): return items.map { $0 }
            case .hallsNearSection(items: let items): return items.map { $0 }
            case .hallSection(items: let items): return items.map { $0 }
            case .activitySection(items: let items): return items.map { $0 }
                case .myChartSection(items: let items): return items.map { $0 }
            }
        }
        
        init(original: SectionModel, items: [Item]) {
            switch original {
            case .hallsNearSection: self = .hallsNearSection(items: items)
            case .hallSection: self = .hallSection(items: items)
            case .activitySection: self = .activitySection(items: items)
            case .chartsSection: self = .chartsSection(items: items)
                case .myChartSection: self = .chartsSection(items: items)
            }
        }
        
        var identity: Identity {
            switch self {
            case .hallsNearSection: return "hallsNearSection"
            case .hallSection: return "hallSection"
            case .activitySection: return "activitySection"
            case .chartsSection: return "chartsSection"
                case .myChartSection: return "myChartSection"
            }
        }
    }
    
    enum ItemModel: Equatable, IdentifiableType {
        case hallsNearItem(ident: String)
        case hallItem(hall: Hour, persent: Int, ident: String)
        case activityItem
        case chartItem(monthes: [Hour])
        case myChartItem(hours: [Hour])
        
        typealias Identity = String
        
        static func ==(lhs: ItemModel, rhs: ItemModel) -> Bool {
            return lhs.identity == rhs.identity
        }
        
        var identity: Identity {
            switch self {
            case let .hallsNearItem(ident): return "hallsNearItem \(ident)"
            case let .hallItem(hall, persent, ident): return "hallItem \(hall.name) \(persent) \(ident)"
            case .activityItem: return "activityItem"
            case let .chartItem(monthes): return "chartItem\(monthes)"
            case let .myChartItem(hours): return "myChartItem\(hours)\(TCLLocalizedStrings.forecast.localized)"
            }
        }
    }
    
}

