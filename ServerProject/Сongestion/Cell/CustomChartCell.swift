//
//  CustomChartCell.swift
//  ServerProject
//
//  Created by Artem Neshko on 15.06.2020.
//  Copyright © 2020 Artem Neshko. All rights reserved.
//

import UIKit

class CustomChartCell: UICollectionViewCell {

    
    @IBOutlet weak var previousLabel: UILabel!
    @IBOutlet weak var forecstLabel: UILabel!
    
    @IBOutlet weak var chartView: ProjectStatusChartView!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setup(hours: [Hour]) {
        let formater = DateFormatter()
        formater.dateFormat = "MM-dd-yyyy HH:mm"
        let dateStr = formater.string(from: Date())
        dateLabel.text = dateStr
        
        previousLabel.text = TCLLocalizedStrings.previous.localized
        forecstLabel.text = TCLLocalizedStrings.forecast.localized
        
        chartView.configure(.red, .green, .amber)
    }

    static func calculateHeight(width: CGFloat) -> CGFloat {
        let newWidth = UIScreen.main.bounds.width - 32
        let height = newWidth / 343.0 * 117.0
        return height + 64
    }
    
}
