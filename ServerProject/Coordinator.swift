//
//  Router.swift
//  techcongress-listener
//
//  Created by Артём Нешко on 30/05/2019.
//  Copyright © 2019 Артём Нешко. All rights reserved.
//

import Foundation
import UIKit

enum Coordinator {
    case month(hall: Month)
    case day(day: Day)
    
    private var storyboardName: String {
        switch self {
        case .month: return "CongestionWeekVC"
        case .day: return "HoursVC"
        }
    }
    
    private var viewController: UIViewController? {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        switch self {
            
        case let .month(hall: hall):
            if let vc = storyboard.instantiateInitialViewController() as? CongestionWeekVC {
                vc.viewModel = CongestionWeekViewModel(hall)
                return vc
            }
            return nil
            
        case let .day(day: day):
            if let vc = storyboard.instantiateInitialViewController() as? HoursVC {
                vc.viewModel = HoursViewModel(day)
                return vc
            }
            return nil
        default: return storyboard.instantiateInitialViewController()
        }
    }
}

extension Coordinator {
    func push(from navigationController: UINavigationController?, animated: Bool = true) {
        
        guard let vc = viewController else { return }
        navigationController?.pushViewController(vc, animated: animated)
    }
    
    func popTo(from navigationController: UINavigationController?, animated: Bool = true) {
        
        guard let vc = viewController else { return }
        navigationController?.pushViewController(vc, animated: animated)
        
        var vcs = navigationController?.viewControllers
        vcs!.remove(at: vcs!.count - 2)
        navigationController?.setViewControllers(vcs!, animated: true)
    }
    
    func present(from viewController: UIViewController?, modalTransitionStyle: UIModalTransitionStyle = .coverVertical, animated: Bool = true, modalPresentationStyle: UIModalPresentationStyle = .none) {
        guard let vc = self.viewController else { return }
        vc.modalPresentationStyle = modalPresentationStyle
        vc.modalTransitionStyle = modalTransitionStyle
        viewController?.present(vc, animated: animated)
    }
    
    func present2(from viewController: UIViewController?, modalTransitionStyle: UIModalTransitionStyle, animated: Bool = true, modalPresentationStyle: UIModalPresentationStyle) {
        guard let vc = self.viewController else { return }
        vc.modalTransitionStyle = modalTransitionStyle
        viewController?.present(vc, animated: animated)
    }
}
